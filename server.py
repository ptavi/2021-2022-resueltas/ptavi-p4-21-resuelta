#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple SIP registrar
"""

import json
import socketserver
import sys
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Simple SIP registrar
    """
    registered = {}

    def __init__(self, *args, **kwargs):
        self.headers = {}
        super().__init__(*args, **kwargs)

    def parse_command(self):
        """Read command (first line) of request.

        Identify command, result, and address, store them in
        instance variables
        """
        elements = self.rfile.readline().decode('utf-8').split()
        self.command = elements[0]
        uri = elements[1]
        if self.command == 'REGISTER':
            if uri.startswith('sip:'):
                self.address = elements[1][4:]
                self.result = "200 OK"
            else:
                self.result = "416 Unsupported URI Scheme"
        else:
            self.result = "405 Method Not Allowed"

    def parse_header(self):
        """Read a SIP header

        Stores result in headers instance dictionary.
        Returns True, or None if no more headers"""

        line = self.rfile.readline().decode('utf-8').rstrip()
        if line == '':
            return False
        elements = line.split(':')
        name = elements[0]
        if name == 'Expires':
            value = int(elements[1])
        else:
            value = elements[1].lstrip()
        self.headers[name] = value
        return True

    def process_register(self):
        """Process REGISTER command"""
        if self.headers['Expires'] == 0:
            if self.address in self.registered:
                del self.registered[self.address]
        else:
            expire_time = time.time() + self.headers['Expires']
            expire_string = time.strftime('%Y-%m-%d %H:%M:%S +0000',
                                          time.gmtime(expire_time))
            self.registered[self.address] = {
                'client': self.client_address[0],
                'expires': expire_string
            }
        self.registered2json()

    def process_request(self):
        """Process request, once command and headers are parsed"""
        if self.command == 'REGISTER':
            self.process_register()

    def handle(self):
        """
        Handle SIP requests
        """
        (sender_ip, sender_port) = self.client_address
        self.parse_command()
        while self.parse_header():
            pass
        self.process_request()
        self.wfile.write(f"SIP/2.0 {self.result}\r\n\r\n".encode())

    @classmethod
    def registered2json(cls):

        with open('registered.json', 'w') as file:
            json.dump(cls.registered, file, indent=4)

    @classmethod
    def json2registered(cls):

        try:
            with open('registered.json', 'r') as file:
                cls.registered = json.load(file)
        except BaseException as exp:
            print(f"Error reading registered.json (exception: {exp})")
            cls.registered = {}


def main():
    if len(sys.argv) < 2:
        sys.exit("Usage: python3 server.py <port>")
    port = int(sys.argv[1])

    try:
        serv = socketserver.UDPServer(('', port), SIPRegisterHandler)
        print(f"Server listening in port {port}")
    except OSError as e:
        sys.exit(f"Error while trying to listen: {e.args[1]}.")

    SIPRegisterHandler.json2registered()

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Server done")
        sys.exit(0)


if __name__ == "__main__":
    main()
