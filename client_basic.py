#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


def main():
    if len(sys.argv) < 4:
        sys.exit("Usage: python3 client_basic.py <ip> <port> <message>")
    sys.argv.pop(0)
    ip = sys.argv.pop(0)
    port = int(sys.argv.pop(0))
    message = ' '.join(sys.argv)

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip, port))
            my_socket.send(message.encode('utf-8'))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
