#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple client for a SIP registrar
"""

import socket
import sys


def main():
    if len(sys.argv) != 6:
        sys.exit("Usage: python3 client.py "
                 + "<ip> <port> register <address> <expiration>")
    sys.argv.pop(0)
    ip = sys.argv.pop(0)
    port = int(sys.argv.pop(0))
    command = sys.argv.pop(0)
    if command == 'register':
        address = sys.argv.pop(0)
        expiration = int(sys.argv.pop(0))
        request = f"REGISTER sip:{address} SIP/2.0\r\n" \
                  + f"Expires: {expiration}\r\n\r\n"

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip, port))
            my_socket.send(request.encode('utf-8'))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
